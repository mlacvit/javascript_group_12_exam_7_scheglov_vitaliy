import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FastFoodComponent } from './fast-food/fast-food.component';
import { DrinksComponent } from './fastFood/drinks/drinks.component';
import { EatComponent } from './fastFood/eat/eat.component';

@NgModule({
  declarations: [
    AppComponent,
    FastFoodComponent,
    DrinksComponent,
    EatComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

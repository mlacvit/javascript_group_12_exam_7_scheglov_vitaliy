import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-eat',
  templateUrl: './eat.component.html',
  styleUrls: ['./eat.component.css']
})
export class EatComponent  {
@Input() eat = '';

}

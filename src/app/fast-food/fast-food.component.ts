import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-fast-food',
  templateUrl: './fast-food.component.html',
  styleUrls: ['./fast-food.component.css']
})

export class FastFoodComponent  {
@Input() drinkOrder: string[] = ['tea', 'coffee', 'cola'];
@Input() eatOrder: string[] = ['cheeseburger', 'hamburger', 'french fries'];
public tea: string[] = [];
public coffee: string[] = [];
public cola: string[] = [];
public cheeseburger: string[] = [];
public hamburger: string[] = [];
public fries: string[] = [];
public price: number[] = [];
public teaPrice = 0;
public coffeePrice = 0;
public colaPrice = 0;
public cheePrice = 0;
public gamPrice = 0;
public friPrice = 0;

addDrink(index: number){
  const teaId = document.getElementById('tea')!;
  const coffeeId = document.getElementById('coffee')!;
  const colaId = document.getElementById('cola')!;
  if (this.drinkOrder[index] === 'tea'){
    this.tea.push('tea');
    this.price.push(50);
    this.teaPrice = 50 + this.teaPrice;
    teaId.innerHTML = `<p>Tea x${this.tea.length} цена: ${this.teaPrice} сом</p>`
  }if (this.drinkOrder[index] === 'coffee'){
  this.coffee.push('coffee');
    this.price.push(70)
    this.coffeePrice = 70 + this.coffeePrice;
    coffeeId.innerHTML = `<p>Coffee x${this.coffee.length} цена: ${this.coffeePrice} сом</p>`
  }if (this.drinkOrder[index] === 'cola'){
  this.cola.push('cola')
    this.price.push(40)
    this.colaPrice = 40 + this.colaPrice;
    colaId.innerHTML = `<p>Cola x${this.cola.length} цена: ${this.colaPrice} сом</p>`
  }
}

addEat(index:number){
  const cheeId = document.getElementById('chee')!;
  const gamId = document.getElementById('gam')!;
  const friId = document.getElementById('fri')!;
  if (this.eatOrder[index] === 'cheeseburger'){
    this.cheeseburger.push('cheeseburger');
    this.price.push(90);
    this.cheePrice = 90 + this.cheePrice;
    cheeId.innerHTML = `<p>Cheeseburger x${this.cheeseburger.length} цена: ${this.cheePrice} сом</p>`
  }if (this.eatOrder[index] === 'hamburger'){
    this.hamburger.push('hamburger');
    this.price.push(80)
    this.gamPrice = 80 + this.gamPrice;
    gamId.innerHTML = `<p>Hamburger x${this.hamburger.length} цена: ${this.gamPrice} сом</p>`
  }if (this.eatOrder[index] === 'french fries'){
    this.fries.push('french fries')
    this.price.push(45)
    this.friPrice = 45 + this.friPrice;
    friId.innerHTML = `<p>french fries x${this.fries.length} цена: ${this.friPrice} сом</p>`
  }
}

prices(){
  return this.price.reduce((acc, num) => acc + num, 0);
}

}
